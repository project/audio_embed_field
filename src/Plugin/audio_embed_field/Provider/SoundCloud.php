<?php

namespace Drupal\audio_embed_field\Plugin\audio_embed_field\Provider;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Utility\Error;
use Drupal\audio_embed_field\ProviderPluginBase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A SoundCloud provider plugin.
 *
 * @AudioEmbedProvider(
 *   id = "soundcloud",
 *   title = @Translation("SoundCloud")
 * )
 */
class SoundCloud extends ProviderPluginBase {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Logger service.
   */
  protected LoggerInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->configFactory = $container->get('config.factory');
    $instance->logger = $container->get('logger.audio_embed_field');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function renderEmbedCode($width, $height, $autoplay) {
    if ($autoplay == 0) {
      $autoplay = 'false';
    }
    if ($autoplay == 1) {
      $autoplay = 'true';
    }
    $embed_code = [
      '#type' => 'audio_embed_iframe',
      '#provider' => 'soundcloud',
      '#url' => sprintf('https://w.soundcloud.com/player/?url=https%%3A//api.soundcloud.com/tracks/%s', $this->getAudioId()),
      '#query' => [
        'auto_play' => $autoplay,
        'visual' => 'true',
        'show_user' => 'false',
        'show_reposts' => 'false',
        'hide_related' => 'true',
        'show_comments' => 'false',
      ],
      '#attributes' => [
        'width' => $width,
        'height' => $height,
        'frameborder' => '0',
      ],
    ];

    return $embed_code;
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteThumbnailUrl() {
    $config = $this->configFactory->get('audio_embed_field.settings');

    $query = [
      'url' => $this->getInput(),
    ];
    if (!empty($config->get('soundcloud_id'))) {
      $query['client_id'] = $config->get('soundcloud_id');
    }
    else {
      return NULL;
    }

    try {
      $client = new Client();
      $res = $client->request('GET', 'https://api.soundcloud.com/resolve.json', [
        'query' => $query,
      ]);
      return json_decode($res->getBody())->artwork_url;
    }
    catch (ClientException $e) {
      if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
        Error::logException($this->logger, $e);
      }
      else {
        // @phpstan-ignore-next-line
        watchdog_exception('audio_embed_field', $e);
      }
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getIdFromInput($input) {
    $soundcloud_id = \Drupal::Config('audio_embed_field.settings')->get('soundcloud_id');
    if (empty($soundcloud_id)) {
      return NULL;
    }
    try {
      $client = new Client();
      $res = $client->request('GET', 'https://api.soundcloud.com/resolve.json', [
        'query' => [
          'url' => $input,
          'client_id' => $soundcloud_id,
        ],
      ]);
      return json_decode($res->getBody())->id;
    }
    catch (ClientException $e) {
      if (version_compare(\Drupal::VERSION, '10.1', '>=')) {
        Error::logException(\Drupal::logger('audio_embed_field'), $e);
      }
      else {
        // @phpstan-ignore-next-line
        watchdog_exception('audio_embed_field', $e);
      }
      return NULL;
    }

  }

}
